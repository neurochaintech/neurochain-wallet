# Neurochain Wallet :credit_card:

Simple UI to execute transations and explore blocks on the Neurochain network.

## Getting Started

You need to have node and yarn installed locally.
Once you downloaded the project, you need to run `yarn` from the project root in order to download all the required dependencies.

### How to deploy

Build the project artifact with `yarn build`.
Then, serve in your web server the artifacts that were generated in the build folder.

### Commands

Build front end artifacts in `build` folder
for wallet

```sh
yarn build
```

for block explorer only

```sh
yarn build:block
```

for faucet

```sh
yarn build:faucet
```

Start front end dev server (add extentions for app type like above)

```sh
yarn start
```

Start mock server

```sh
yarn mock
```

Run tests

```sh
yarn test
```
