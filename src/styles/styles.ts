import { injectGlobal } from "emotion";
/* tslint:disable */
injectGlobal`
    body {
        margin: 0px;
        padding: 0;
        
        font-family: 'Roboto', sans-serif;
        * {
            box-sizing: border-box;
        }
        color: dimgray;
        background: white;
    };
    html {
        font-size: 16px;
    };
`;
/* tslint:enable */
