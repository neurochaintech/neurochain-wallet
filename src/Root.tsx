import * as React from 'react';
import '../src/styles/custom-toastr.css';
import './App.css';
import './styles/styles';

const App = require('./components/App/App').default


class Root extends React.Component {
  public render() {
    return (
      <App />
    )
  }
}

export default Root
