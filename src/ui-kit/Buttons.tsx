import React from 'react'
import styled from 'react-emotion'
import { Link } from 'react-router-dom'
import { theme } from '../styles/theme'

const tinycolor = require('tinycolor2')

export const fluidStyle = (props: { fluid?: boolean }) => ({
  width: props.fluid ? '100%' : ''
})

export const pointer = { cursor: 'pointer' }

const myButtonPseudos = (color: string) =>
  `
:hover{
transition: background-color 0.15s ease;
     background: ${tinycolor(color)
       .darken(10)
       .toString()};
    };
 :disabled {
    background: ${tinycolor(color)
      .lighten(20)
      .toString()};;
    cursor: not-allowed;   
};
:focus {outline:0;}    
`

const Button = styled('button')(
  props => ({
    background: props.theme.primary,
    fontSize: '1em',
    padding: '0.8em',
    color: 'white',
    borderRadius: 5,
    cursor: 'pointer'
  }),
  props => {
    const color = props.theme.primary

    return myButtonPseudos(color)
  },
  fluidStyle
)

export const DressedButton = styled(Button)({
  margin: '10px 0px',
  padding: 14,
  borderRadius: 5,
  fontSize: '0.8em',
  boxShadow: '0 10px 20px 0 rgba(0,0,0,0.20)'
})

const mainColor = theme.primary

export const RoundButton = styled('button')(
  props => ({
    padding: 20,
    border: 'none',
    fontSize: '1em',
    background: mainColor,
    borderRadius: 40,
    color: 'white'
  }),
  myButtonPseudos(mainColor),
  pointer
)

export const commonInputStyles = {
  border: '1px solid lightgray',
  borderRadius: 5,
  fontSize: '1em',
  color: 'gray'
}

export const InputRound = styled('input')(
  {
    ...commonInputStyles,
    padding: '0.8em',
    transition: 'box-shadow 0.3s;'
  },
  fluidStyle,
  `
    :focus {
        outline: none;
        box-shadow: 0 0 3pt 2pt ${theme.primary};
    
}
::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: lightgray;
    opacity: 1; /* Firefox */
}
`
)

export const RoundButtonWithLoader = props => (
  <RoundButton {...props}>
    {props.isLoading ? 'Please wait...' : props.children}
  </RoundButton>
)

export const StyledLink = styled(Link)`
  text-decoration: none;
  color: dimgray;
  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`
export const StyledLinkFancy = styled(StyledLink)({
  color: theme.primary
  // textDecoration: 'underline!important'
})

export const LightButton = styled('button')(
  (props: { color: string; disabled?: boolean }) => {
    const disabledColor = tinycolor(props.color)
      .lighten(20)
      .toString()

    const displayedColor = props.disabled ? disabledColor : props.color
    return {
      padding: '0.8em',
      color: displayedColor,
      border: `1px ${displayedColor} solid`,
      borderRadius: 5,
      cursor: props.disabled ? 'not-allowed' : 'pointer',
      width: '100%',
      display: 'inline-block',
      textAlign: 'center',
      fontSize: '1em',
      background: 'white',
      transition: 'all .2s ease-in'
    }
  },
  `
  :hover:enabled {
    background: ${theme.primary};
    color: white;
  }
  `
)
