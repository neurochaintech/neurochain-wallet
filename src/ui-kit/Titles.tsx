import styled from 'react-emotion'

export const Title = styled('h1')({
  textAlign: 'center',
  margin: '50px 0'
})
