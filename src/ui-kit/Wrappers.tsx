import styled, { keyframes } from 'react-emotion'

export const CenterWrapper = styled('div')({
  alignItems: 'center',
  justifyContent: 'center',
  display: 'flex',
  height: '100vh'
})

export const VanillaContainer = styled('div')`
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  @media (min-width: 768px) {
    width: 750px;
  }
  @media (min-width: 992px) {
    width: 970px;
  }
  // @media (min-width: 992px) {
  //   width: 1170px;
  // }
  @media (min-width: 1200px) {
    width: 1170px;
  }
`

export const myShadow = '0 2px 10px 0 rgba(0,0,0,0.13)'

export const CardBorder = styled('div')({
  // border: '1px solid lightgray',
  boxShadow: myShadow,

  borderRadius: '0%'
})

const fade = keyframes`
from { opacity: 0; }
to   { opacity: 1; }
`

export const fadeEffect = `animation: ${fade} 1s ease;`

export const FadeIn = styled('div')(fadeEffect)
