// import raf from 'tempPolyfills'
import Enzyme, { mount, render, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
// import toJson from 'enzyme-to-json'

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() })

// // Make Enzyme functions available in all test files without importing
// // global.shallow = shallow
// // global.render = render
// // global.mount = mount
// // global.toJson = toJson

// // Fail tests on any warning

const localStorageMock = {
  clear: jest.fn(),
  getItem: jest.fn(),
  setItem: jest.fn()
}
;(global as any).localStorage = localStorageMock
