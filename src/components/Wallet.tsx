import { execTrans, getIncTrans, IRecord } from 'nc-utils'
import React, { Component } from 'react'
import { Col, Row } from 'react-flexbox-grid'
import FaldeLoader from 'react-spinners/FadeLoader'
import toastr from 'toastr'
import { deleteKey, getKey } from '../services/storage'
import { theme } from '../styles/theme'
import { InputRound, pointer, RoundButtonWithLoader } from '../ui-kit/Buttons'
import { Title } from '../ui-kit/Titles'
import { VanillaContainer } from '../ui-kit/Wrappers'
import { getTotal, Incoming } from './Incoming'
import { Outgoing } from './Outgoing'

toastr.options = {
  closeButton: false,
  debug: false,
  newestOnTop: false,
  progressBar: false,
  positionClass: 'toast-bottom-full-width',
  preventDuplicates: false,
  showEasing: 'swing',
  hideEasing: 'linear',
  showMethod: 'fadeIn',
  hideMethod: 'fadeOut'
}

const Spinner = () => (
  <div style={{ display: 'flex', justifyContent: 'center' }}>
    <FaldeLoader color={theme.primary} />
  </div>
)

export const updateIncoming = (
  id,
  checked,
  incomingTrans: IRecordWithCheck[]
) => {
  const idToChange = incomingTrans.findIndex(el => el.id === id)
  const newEl = { ...incomingTrans[idToChange], checked }

  const result = [...incomingTrans]
  result[idToChange] = newEl

  return result
}

export interface IRecordWithCheck extends IRecord {
  checked?: boolean
}

export const canIssueTransaction = (
  incoming: IRecordWithCheck[],
  outgoing: IRecord[],
  privateKey: string
) => {
  const totalOut = getTotal(outgoing)
  const tottalInc = getTotal(incoming)
  const MIN_KEY_LENGTH = 6
  return !!(
    tottalInc &&
    totalOut &&
    totalOut <= tottalInc &&
    privateKey.length > MIN_KEY_LENGTH
  )
}

export const uncheckEls = (els: IRecordWithCheck[]) =>
  els.forEach(el => {
    el.checked = false
  })

const logout = () => {
  deleteKey()
  window.location.replace('/enterKey')
}

export const removeOutgoingTrans = (id: string, data: any[]) => {
  const index = data.findIndex(el => el.id === id)
  const newArray = [...data]
  newArray.splice(index, 1)
  return newArray
}

export const Logout = () => (
  <span style={{ ...pointer }} onClick={logout}>
    Change address…
  </span>
)

export default class Wallet extends Component<any, any> {
  state = {
    incoming: [] as IRecordWithCheck[],
    outgoing: [] as IRecord[],
    fetchIsLoading: false,
    saveIsLoading: false,
    privateKey: ''
  }

  componentDidMount() {
    this.getIncTrans()
  }

  /**
   * Load incoming transactions
   */
  getIncTrans() {
    this.setState({ fetchIsLoading: true })
    getIncTrans(getKey() as string)
      .then(data => {
        uncheckEls(data)
        this.setState({ incoming: data })
        this.setState({ fetchIsLoading: false })
      })
      .catch(err => {
        toastr.error('Failed to load incoming transactions')
        this.setState({ fetchIsLoading: false })
      })
  }

  /**
   * Save transaction
   */
  execTrans = e => {
    e.preventDefault()
    this.setState({ saveIsLoading: true })

    const { incoming, outgoing, privateKey } = this.state
    const incAdresses = incoming.map(el => el.id)
    execTrans({
      incoming: incAdresses,
      outgoing,
      privateKey
    })
      .then(res => {
        toastr.info(
          `You successfully sent ${getTotal(this.state.outgoing)} NCC`
        )
        this.setState(
          { saveIsLoading: false, incoming: [], outgoing: [] },
          () => {
            this.getIncTrans()
          }
        )
      })
      .catch(err => {
        toastr.error('Failed to execute transaction')
      })
  }

  /**
   * Handle the data change from the incoming table when user checks boxes
   */
  handleIncomingChange = (id: string, checked: boolean) => {
    const newIncoming = updateIncoming(id, checked, this.state.incoming)

    this.setState({
      incoming: newIncoming
    })
  }

  /**
   * Handle the data change from the outgoing table when user adds transactions (add button)
   */
  handleOutgoingSubmit = record => {
    this.setState({ outgoing: [...this.state.outgoing, record] })
  }

  /**
   * Handle the data change from the outgoing table when user removes transactions (red cross button)
   */
  handleOutgoingRemove = (id: string) => {
    this.setState({ outgoing: removeOutgoingTrans(id, this.state.outgoing) })
  }

  handlePkChange = e => {
    const pk = e.target.value

    this.setState({ privateKey: pk })
  }

  render() {
    return (
      <VanillaContainer
        style={{
          flex: 1
        }}
      >
        <Title>
          My Wallet
          <span style={{ margin: '0 10px' }} className="ec ec-credit-card" />
        </Title>

        {this.state.fetchIsLoading ? (
          <Spinner />
        ) : (
          <form
            onSubmit={this.execTrans}
            style={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between'
            }}
          >
            <Row>
              <Col xs={12} md={6}>
                <Incoming
                  // For display, shorten strings that are too long
                  data={this.state.incoming}
                  onChange={this.handleIncomingChange}
                  insufficientFunds={
                    getTotal(this.state.incoming) <
                    getTotal(this.state.outgoing)
                  }
                />
              </Col>
              <Col xs={12} md={6}>
                <Outgoing
                  onDelete={this.handleOutgoingRemove}
                  onAddRecord={this.handleOutgoingSubmit}
                  data={this.state.outgoing}
                />
              </Col>
              <Col xs={12}>
                <div style={{ textAlign: 'center', margin: '60px 0' }}>
                  <div>
                    Private Key
                    <span
                      className="ec ec-closed-lock-with-key"
                      style={{ margin: '0 8px' }}
                    />
                  </div>
                  <InputRound
                    type="password"
                    placeholder="Your private key"
                    style={{ margin: '10px 0' }}
                    value={this.state.privateKey}
                    onChange={this.handlePkChange}
                  />
                </div>
              </Col>
            </Row>
            <RoundButtonWithLoader
              type="submit"
              style={{ width: 200, margin: '50px auto' }}
              isLoading={this.state.saveIsLoading}
              disabled={
                !canIssueTransaction(
                  this.state.incoming,
                  this.state.outgoing,
                  this.state.privateKey
                )
              }
            >
              Send!
            </RoundButtonWithLoader>
          </form>
        )}
      </VanillaContainer>
    )
  }
}
