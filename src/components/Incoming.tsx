import React, { SFC } from 'react'
import styled, { css } from 'react-emotion'
import { CardBorder } from '../ui-kit/Wrappers'
import { IRecordWithCheck } from './Wallet'

const MyTable = styled('table')({ width: '100%', borderCollapse: 'collapse' })

interface IRecordProps extends IRecordWithCheck {
  onChange: (id: string, selected: boolean) => void
  onDelete?: (id: string) => void
}
export const MyInput = props => {
  return (
    <input checked={props.selected} type="checkbox" onChange={props.onChange} />
  )
}

export const MyTd = styled('td')({ padding: 15 })

export const MyRecord: SFC<IRecordProps> = ({
  id,
  amount,
  checked,
  onChange,
  onDelete = () => {}
}) => (
  <tr>
    <MyTd>{id.substr(0, 20)}…</MyTd>
    <MyTd>{amount}</MyTd>
    <MyTd style={{ textAlign: 'center' }}>
      {checked !== undefined ? (
        <MyInput checked={checked} onChange={() => onChange(id, !checked)} />
      ) : (
        <span
          onClick={() => onDelete(id)}
          className="ec ec-x"
          style={{ fontSize: '0.7em', cursor: 'pointer' }}
        />
      )}
    </MyTd>
  </tr>
)

export const getTotal = (data: IRecordWithCheck[]) => {
  let total = 0

  data.forEach(el => {
    if (el.checked || el.checked === undefined) {
      total += el.amount
    }
  })
  return total
}

export const MyTh = styled('th')({ textAlign: 'left', padding: 15 })

export const TableWithBorder = styled(MyTable)(
  css(`
tr { border-bottom: 1px solid lightgray; }
tr:last-child { border-bottom: none; }
`)
)

export const MyTbody = styled('tbody')(
  (props: { even?: boolean }) => css`
    tr:nth-child(${props.even ? 'even' : 'odd'}) {
      background-color: #e5eefc;
    }
  `
)

export const Incoming: SFC<{
  data: IRecordWithCheck[]
  onChange: any
  insufficientFunds: boolean
}> = ({ data, onChange, insufficientFunds }) => {
  const total = getTotal(data)

  return (
    <div>
      <Total danger={insufficientFunds} style={{ fontSize: '1.3em' }}>
        Total: {total.toString()} NCC
      </Total>
      <CardBorder>
        <TableWithBorder>
          <MyTbody>
            <tr>
              <MyTh>Id</MyTh>
              <MyTh>Amount</MyTh>
              <MyTh>Selected</MyTh>
            </tr>
            {data.map(record => (
              <MyRecord key={record.id} {...record} onChange={onChange} />
            ))}
          </MyTbody>
        </TableWithBorder>
      </CardBorder>
    </div>
  )
}

export const Total = styled('div')((props: { danger?: boolean }) => ({
  color: props.danger ? 'red' : 'inherit',
  fontSize: '1.3em',
  margin: '15px 0'
}))
