import { formIsValid } from './Outgoing'

test('Form is valid should return true if input is valie', () => {
  expect(formIsValid('foooo', 33, [])).toEqual(false)
  expect(formIsValid('fooooaaaaaaaaa', 33, [])).toEqual(true)
  expect(formIsValid('fooooaaaaaaaaa', 0, [])).toEqual(false)
  expect(
    formIsValid('long_enough_taken', 33, [{ id: 'long_enough_taken' }])
  ).toEqual(false)
})
