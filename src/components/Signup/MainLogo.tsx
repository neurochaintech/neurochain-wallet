import React from 'react'
import styled from 'react-emotion';

const logo = require('../../assets/logo-neurochain-new.png')

const MainTitleFont = styled('h1')({
    fontFamily: "'Montserrat', sans-serif",
    fontSize: '4em',
    textAlign: 'center',
    margin: '50px 20px 50px 20px'
   
})

export default function MainLogo() {
  return (
    <div style={{display: 'flex', alignItems: 'center'}}>
      <img height={55} src={logo}

      
      />
      <MainTitleFont>
    NEUROCHAIN
 
    </MainTitleFont></div>
  )
}
