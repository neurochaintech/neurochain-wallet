import React, { Component } from 'react'
import styled from 'react-emotion'
import { Link } from 'react-router-dom'
import { saveKey } from '../../services/storage'
import { InputRound, RoundButton, StyledLink } from '../../ui-kit/Buttons'
import SessionContext from '../Session/sessionContext'
import { KeyTitle } from './GenKey'
import MainLogo from './MainLogo';

const MyButton = styled(RoundButton)({ display: 'block', width: 200 })

export const MainWrapper = styled('div')({
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',

})

const MIN_KEY_LENGTH = 6
const canSubmitKey = (key: string) => key.length > MIN_KEY_LENGTH

export default class EnterKey extends Component<any, any> {
  public state = {
    publicKey: ''
  }

  handlePubKeyChange = e => {
    this.setState({ publicKey: e.target.value })
  }

  public render() {
    return (
      <SessionContext.Consumer>
        {({ saveAddress }) => (
          <MainWrapper>
            <MainLogo />
            <KeyTitle>Enter your address...</KeyTitle>
            <InputRound
            style={{margin: '20px 0'}}
              placeholder="Your address"
              onChange={this.handlePubKeyChange}
              value={this.state.publicKey}
            />
            <StyledLink to="/newKeys"
                  style={{margin: '20px 0'}}
            >or create new keys...</StyledLink>

            <Link to="/wallet" style={{ textDecoration: 'none' }}>
              <MyButton
                style={{margin: '20px 0'}}
                id="submit"
                name="submit"
                disabled={!canSubmitKey(this.state.publicKey)}
                onClick={() => saveAddress(this.state.publicKey)}
              >
                Submit!
              </MyButton>
            </Link>
          </MainWrapper>
        )}
      </SessionContext.Consumer>
    )
  }
}
