import { genKeys } from 'nc-utils'
import { IKeyPair } from 'nc-utils'
import React, { Component } from 'react'
import styled from 'react-emotion'
import FadeLoader from 'react-spinners/FadeLoader'
import toastr from 'toastr'
import { saveKey } from '../../services/storage'
import { theme } from '../../styles/theme'
import { RoundButton, StyledLink } from '../../ui-kit/Buttons'
import SessionContext from '../Session/sessionContext'
import { MainWrapper } from './EnterKey'
import MainLogo from './MainLogo';

export const KeyTitle = styled('h2')({
  fontSize: '2.1em',
  textAlign: 'center'
  
})

export const SmallKeyTitle = styled('h2')({
  fontSize: '1.2em'
})

const MyButton = styled(RoundButton)({ display: 'block', width: 200 })

const KeysDisplay = ({ keys }) => (
  <div style={{ textAlign: 'center', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
    <KeyTitle>
      Write down your keys.. <span className="ec ec-old-key" />
    </KeyTitle>

    <SmallKeyTitle>Your private key:</SmallKeyTitle>
    <div style={{ wordBreak: 'break-word', width: 500 }}>{keys.private}</div>
    <SmallKeyTitle>Your address:</SmallKeyTitle>
    <div style={{ wordBreak: 'break-word', width: 500 }}>{keys.adress}</div>
    <StyledLink to="/wallet" style={{ display: 'block', margin: '30px 0' }}>
      Go to your wallet... <span className="ec ec-gem" />
    </StyledLink>
  </div>
)

const GeneratingKey = () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    }}
  >
    <KeyTitle> Generating your Key...</KeyTitle>
    <FadeLoader color={theme.primary} />
  </div>
)

interface IState {
  keys: IKeyPair | null
  isLoading: boolean
}

export class GenKey extends Component<any, IState> {
  public state = {
    keys: null,
    isLoading: false
  }

  componentDidMount() {
    this.generateKeys()
  }

  generateKeys = () => {
    this.setState({ isLoading: true })
    genKeys()
      .then(keys => {
        this.setState({ keys })
        this.setState({ isLoading: false })
        console.info('your address is: ', keys.adress)
        console.info('your private key is: ', keys.private)
        this.props.saveAddress(keys.adress)
        /**
         * save address in local storage
         */
        saveKey(keys.adress)
      })
      .catch(err => {
        toastr.error('failed to generate keys !')
        console.error(err)
        this.setState({ isLoading: false })
      })
  }

  render() {
    const { keys } = this.state
    return (
      <MainWrapper>

        <div style={{minHeight: 600}}><MainLogo />
        {this.state.isLoading ? (
        <GeneratingKey />
        ) : keys ? (
        <KeysDisplay keys={keys} />
        ) : (
        <KeyTitle>Generate your key...</KeyTitle>
        )}</div>
        <MyButton disabled={this.state.isLoading} onClick={this.generateKeys}>
          Generate!
        </MyButton>
      </MainWrapper>
    )
  }
}

export default props => (
  <SessionContext.Consumer>
    {({ saveAddress }) => <GenKey saveAddress={saveAddress} {...props} />}
  </SessionContext.Consumer>
)
