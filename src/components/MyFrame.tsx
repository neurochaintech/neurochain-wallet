import React from 'react'
import Footer from './Footer'
import Header from './Header'
import HeaderMinimal from './HeaderMinimal'

export const MyFrame = ({ children }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      minHeight: '100vh'
    }}
  >
    <Header />
    {children}
    <Footer />
  </div>
)

export const MyFrameMinimal = ({ children }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      minHeight: '100vh'
    }}
  >
    <HeaderMinimal />
    {children}
    <Footer />
  </div>
)
