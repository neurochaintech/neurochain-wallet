import React from 'react'

const version = require('../services/version')
export default () => (
  <div
    style={{
      marginTop: 60,
      padding: 30,
      width: 600,
      borderTop: '1px solid lightgray',
      textAlign: 'center'
    }}
  >
    <div style={{ fontSize: '0.9em' }}>Neurochain 2018</div>
    <div style={{ fontSize: '0.8em' }}>v{version}</div>
  </div>
)
