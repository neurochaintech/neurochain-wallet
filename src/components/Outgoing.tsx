import React, { Component } from 'react'
import { theme } from '../styles/theme'
import { InputRound, LightButton } from '../ui-kit/Buttons'
import { CardBorder } from '../ui-kit/Wrappers'
import {
  getTotal,
  MyRecord,
  MyTbody,
  MyTd,
  MyTh,
  TableWithBorder,
  Total
} from './Incoming'

export const formIsValid = (recipient: string, nccQty: number, outgoing) => {
  return Boolean(
    recipient.length > 8 && nccQty && !outgoing.some(el => el.id === recipient)
  )
}

export class Outgoing extends Component<any, any> {
  public state = { id: '', amount: '' } as any

  handleChange = e => {
    const key = e.target.id
    const data = e.target.value
    const value = e.target.type === 'number' ? parseInt(data, 10) : data
    this.setState({ [key]: value })
  }

  onAddRecord = e => {
    this.props.onAddRecord(this.state)
    this.setState({ id: '', amount: '' })
  }

  render() {
    const { data } = this.props

    const total = getTotal(data)

    return (
      <div>
        <Total style={{ fontSize: '1.3em' }}>
          Total: {total.toString()} NCC
        </Total>
        <CardBorder>
          <TableWithBorder>
            <MyTbody>
              <tr>
                <MyTh style={{ width: '40%' }}>Id</MyTh>
                <MyTh style={{ width: '40%' }}>Amount</MyTh>
                <MyTh style={{ width: '20%' }} />
              </tr>
              {data.map(record => (
                <MyRecord
                  onDelete={this.props.onDelete}
                  key={record.id}
                  {...record}
                />
              ))}

              <tr style={{ background: 'none' }}>
                <MyTd>
                  <InputRound
                    style={{ width: '100%' }}
                    id="id"
                    placeholder="Enter recipient"
                    type="text"
                    value={this.state.id}
                    onChange={this.handleChange}
                  />
                </MyTd>
                <MyTd>
                  <InputRound
                    style={{ width: '100%' }}
                    id="amount"
                    placeholder="Enter amount of NCC"
                    type="number"
                    value={this.state.amount}
                    onChange={this.handleChange}
                  />
                </MyTd>
                <MyTd>
                  <LightButton
                    style={{ width: '100%' }}
                    onClick={this.onAddRecord}
                    color={theme.primary}
                    disabled={
                      !formIsValid(
                        this.state.id,
                        this.state.amount,
                        this.props.data
                      )
                    }
                  >
                    Add
                  </LightButton>
                </MyTd>
              </tr>
            </MyTbody>
          </TableWithBorder>
        </CardBorder>
      </div>
    )
  }
}
