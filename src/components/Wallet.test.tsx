import {
  canIssueTransaction,
  removeOutgoingTrans,
  updateIncoming
} from './Wallet'

test('Update incoming transactions should generate an array with updated values', () => {
  const mockIncTrans = [
    { id: 'banana', amount: 22, checked: true },
    { id: 'cool', amount: 1, checked: false }
  ]
  expect(updateIncoming('cool', true, mockIncTrans)).toEqual([
    { id: 'banana', amount: 22, checked: true },
    { id: 'cool', amount: 1, checked: true }
  ])
  expect(updateIncoming('banana', false, mockIncTrans)).toEqual([
    { id: 'banana', amount: 22, checked: false },
    { id: 'cool', amount: 1, checked: false }
  ])
  expect(updateIncoming('banana', true, mockIncTrans)).toEqual([
    { id: 'banana', amount: 22, checked: true },
    { id: 'cool', amount: 1, checked: false }
  ])
})

test('canIssueTransaction should return true if balance is correct and private key is entered', () => {
  let mockIncoming = [
    { id: 'banana', amount: 22, checked: true },
    { id: 'cool', amount: 5, checked: true }
  ]

  let mockOutgoing = [{ id: 'banana', amount: 22 }, { id: 'cool', amount: 10 }]

  expect(canIssueTransaction(mockIncoming, mockOutgoing, 'longenough')).toEqual(
    false
  )

  mockIncoming = [
    { id: 'banana', amount: 22, checked: true },
    { id: 'cool', amount: 5, checked: true }
  ]

  mockOutgoing = [{ id: 'banana', amount: 22 }, { id: 'cool', amount: 1 }]

  expect(canIssueTransaction(mockIncoming, mockOutgoing, 'longEnough')).toEqual(
    true
  )
  expect(canIssueTransaction(mockIncoming, mockOutgoing, 'short')).toEqual(
    false
  )

  mockIncoming = [
    { id: 'banana', amount: 22, checked: false },
    { id: 'cool', amount: 5, checked: false }
  ]

  mockOutgoing = [{ id: 'banana', amount: 22 }, { id: 'cool', amount: 1 }]

  expect(canIssueTransaction(mockIncoming, mockOutgoing, 'longEnough')).toEqual(
    false
  )
})

test('remove transaction should remove trasation with passed id', () => {
  const mockOutgoing = [
    { id: 'banana', amount: 22 },
    { id: 'cool', amount: 10 }
  ]

  expect(removeOutgoingTrans('banana', mockOutgoing)).toEqual([
    { id: 'cool', amount: 10 }
  ])
})
