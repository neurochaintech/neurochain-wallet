import React, { Component } from 'react'
import { getKey, saveKey } from '../../services/storage'
import { SessionContext } from './sessionContext'

export default class Session extends Component {
  state = {
    address: getKey()
  }

  saveAddress = (address: string) => {
    this.setState({ address })
    saveKey(address)
  }

  render() {
    return (
      <SessionContext.Provider
        value={{
          address: this.state.address as string,
          saveAddress: this.saveAddress
        }}
      >
        {this.props.children}
      </SessionContext.Provider>
    )
  }
}
