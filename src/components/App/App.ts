if (process.env.REACT_APP_TYPE === 'block-explorer') {
  module.exports = require('./BlockExplorer')
} else if (process.env.REACT_APP_TYPE === 'faucet') {
  module.exports = require('./FaucetApp')
} else {
  module.exports = require('./WalletApp')
}
