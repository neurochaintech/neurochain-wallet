import * as React from 'react'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import Block from '../Block'
import BlockExplorer from '../BlockExplorer/BlockExplorer'
import { MyFrame, MyFrameMinimal } from '../MyFrame'
import Transaction from '../Transaction'

class App extends React.Component {
  public render() {
    return (
      <Router>
        <MyFrameMinimal>
          <Switch>
            <Route path="/blockExplorer" component={BlockExplorer} />
            <Route path="/block/:id" component={Block} />
            {/* / id+ to allow base64 urls...*/}
            <Route path="/transaction/:id+" component={Transaction} />
            <Redirect to="/blockExplorer" />
          </Switch>
        </MyFrameMinimal>
      </Router>
    )
  }
}

export default App
