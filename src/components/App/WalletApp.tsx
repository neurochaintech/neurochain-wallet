import * as React from 'react'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import { PrivateRoute, PublicRoute } from '../AuthRoutes'
import Block from '../Block'
import BlockExplorer from '../BlockExplorer/BlockExplorer'
import { MyFrame } from '../MyFrame'
import Session from '../Session/Session'
import EnterKey from '../Signup/EnterKey'
import GenKey from '../Signup/GenKey'
import Transaction from '../Transaction'
import Wallet from '../Wallet'

class App extends React.Component {
  public render() {
    return (
      <Session>
        <Router>
          <MyFrame>
            <Switch>
              <PublicRoute path="/enterKey" component={EnterKey} />
              <PublicRoute path="/newKeys" component={GenKey} />
              <PrivateRoute path="/wallet" component={Wallet} />
              <Route path="/blockExplorer" component={BlockExplorer} />
              <Route path="/block/:id" component={Block} />
              {/* / id+ to allow base64 urls...*/}
              <Route path="/transaction/:id+" component={Transaction} />
              <Redirect to="/wallet" />
            </Switch>
          </MyFrame>
        </Router>
      </Session>
    )
  }
}

export default App
// module.exports = App
