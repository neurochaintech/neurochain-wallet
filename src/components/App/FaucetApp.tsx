import * as React from 'react'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import Faucet from '../Faucet'
import { MyFrameMinimal } from '../MyFrame'

class App extends React.Component {
  public render() {
    return (
      <Router>
        <MyFrameMinimal>
          <Switch>
            <Route path="/faucet" component={Faucet} />
            <Redirect to="/faucet" />
          </Switch>
        </MyFrameMinimal>
      </Router>
    )
  }
}

export default App
