import React from 'react'
import styled from 'react-emotion'
import { SessionContext } from '../components/Session/sessionContext'
import { theme } from '../styles/theme'
import { StyledLink } from '../ui-kit/Buttons'
import { myShadow, VanillaContainer } from '../ui-kit/Wrappers'
import { Logout } from './Wallet'

const logo = require('../assets/logo-neurochain-new.png')

export const HeaderBar = styled('div')({
  height: 50,
  width: '100%',
  background: `linear-gradient(90deg, #36D7B7, ${theme.primary});`,
  boxShadow: myShadow
})

const LeftSection = styled('div')({
  display: 'flex',
  alignItems: 'center'
})

const MyLink = styled(StyledLink)(
  { marginRight: 20 },
  `
  transition: all 0.3s;
  :hover {
    color: ${theme.primary};
  }
`
)

const MyAddress = ({ address }) => (
  <span style={{ margin: '0 20px' }}>{`Your address: ${address.slice(
    0,
    80
  )}`}</span>
)

export default props => {
  return (
    <SessionContext.Consumer>
      {({ address }) => (
        <HeaderBar {...props}>
          <VanillaContainer
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              height: '100%'
            }}
          >
            <LeftSection>
              <img src={logo} height={'30px'} style={{ marginRight: 20 }} />
              <MyLink to="/blockExplorer">Block Explorer</MyLink>
              {address && <MyLink to="/wallet">Wallet</MyLink>}
            </LeftSection>

            <div>
              {address && <MyAddress address={address} />}
              {address && <Logout />}
              {!address && (
                <MyLink style={{}} to="/enterKey">
                  Enter adress...
                </MyLink>
              )}
            </div>
          </VanillaContainer>
        </HeaderBar>
      )}
    </SessionContext.Consumer>
  )
}
