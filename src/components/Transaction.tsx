import { getTransaction } from 'nc-utils'
import React, { Component } from 'react'
import { Title } from '../ui-kit/Titles'
import { VanillaContainer } from '../ui-kit/Wrappers'

interface IProps {
  match: any
}

export default class Transaction extends Component<IProps> {
  state = {
    transaction: {}
  }

  componentDidMount() {
    const transId = decodeURI(this.props.match.params.id)
    getTransaction(transId)
      .then(transaction => {
        this.setState({ transaction })
      })
      .catch(err => {})
  }

  render() {
    return (
      <VanillaContainer style={{ flex: 1, wordBreak: 'break-word' }}>
        <Title>
          Transaction <span className="ec ec-zap" />
        </Title>
        {JSON.stringify(this.state.transaction)}
      </VanillaContainer>
    )
  }
}
