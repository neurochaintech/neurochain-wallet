import axios from 'axios'
import React, { Component } from 'react'
import styled from 'react-emotion'
import { InputRound, RoundButton } from 'src/ui-kit/Buttons'
import { Title } from 'src/ui-kit/Titles'
import toastr from 'toastr'

const MyButton = styled(RoundButton)({ display: 'block', margin: '60px auto' })

export default class Faucet extends Component {
  state = { address: '' }

  handleAddressChangte = e => {
    this.setState({ address: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    axios
      .get('faucet_send', {
        params: {
          address: encodeURI(this.state.address)
        }
      })
      .then(res => {
        toastr.info('NCC sent !')
      })
      .catch(err => {
        console.error(err)
      })
  }

  render() {
    return (
      <div style={{ flex: 1 }}>
        <Title>
          Faucet
          <span style={{ margin: '0 10px' }} className="ec ec-credit-card" />
        </Title>
        <form onSubmit={this.handleSubmit}>
          <InputRound
            placeholder="Enter NCC recipient address..."
            onChange={this.handleAddressChangte}
            value={this.state.address}
            style={{ margin: 'auto', display: 'block', width: 250 }}
          />

          <MyButton type="submit" disabled={this.state.address.length < 8}>
            Send NCC !
          </MyButton>
        </form>
      </div>
    )
  }
}
