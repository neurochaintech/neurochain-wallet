import React from 'react'

import { Redirect, Route } from 'react-router-dom'
import { getKey } from '../services/storage'

/**
 * Redirect to enterKey if user has no key in localstorage
 */
export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      getKey() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/enterKey', state: { from: props.location } }}
        />
      )
    }
  />
)

/**
 * Redirect to dashboard if user has address in localstorage
 */
export const PublicRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !getKey() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/wallet', state: { from: props.location } }}
        />
      )
    }
  />
)
