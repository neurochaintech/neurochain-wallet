import { getBlock } from 'nc-utils'
import React, { Component } from 'react'
import { StyledLinkFancy } from '../ui-kit/Buttons'
import { Title } from '../ui-kit/Titles'
import { VanillaContainer } from '../ui-kit/Wrappers'

export const Transaction = ({ transaction }) => (
  <div>
    <StyledLinkFancy to={'/transaction/' + encodeURI(transaction.id.data)}>
      id: {transaction.id.data}
    </StyledLinkFancy>
  </div>
)

interface IProps {
  match: { params: { id: number } }
}

interface IState {
  block: any
}

export default class Block extends Component<IProps, IState> {
  state = {
    block: {}
  }
  componentDidMount() {
    getBlock(this.props.match.params.id)
      .then(block => {
        console.log(block)
        this.setState({ block })
      })
      .catch(err => {
        console.error(err)
      })
  }
  render() {
    const { header, transactions = [] } = this.state.block as any
    return (
      <VanillaContainer style={{ wordWrap: 'break-word', flex: 1 }}>
        <Title>Block {this.props.match.params.id}</Title>
        <div>
          <h2>header</h2>
          {JSON.stringify(header)}
        </div>
        <div>
          <h2>transactions</h2>
          {transactions.map(transaction => (
            <Transaction key={transaction.id.data} transaction={transaction} />
          ))}
        </div>
      </VanillaContainer>
    )
  }
}
