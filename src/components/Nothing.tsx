import React, { Component } from 'react'

export default class Nothing extends Component {
  render() {
    return (
      <div>
        <h1>Nothing here</h1>
      </div>
    )
  }
}
