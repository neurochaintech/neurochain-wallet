import { getBlock, getTransaction } from 'nc-utils'
import React, { Component } from 'react'
import styled from 'react-emotion'
import { Redirect } from 'react-router-dom'
import BeatLoader from 'react-spinners/BeatLoader'
import * as _ from 'underscore'
import { InputRound } from '../../ui-kit/Buttons'
const MyInput = styled(InputRound)({ width: 300 })

const MyLoader = () => (
  <div style={{ margin: 25 }}>
    <BeatLoader size={10} />
  </div>
)

export const isNumber = (val: any) => typeof val === 'number'

interface IState {
  search: string
  result: {
    block
    transaction
  }
  isLoading: boolean
  isTyping: boolean
}

export const NoResult = ({ children }) => (
  <div style={{ color: '#c27575', margin: 18 }}>no results for {children}</div>
)

const ncService = { getBlock, getTransaction }

export default class Search extends Component<any, IState> {
  state = {
    search: '',
    result: {
      block: null,
      transaction: null
    },
    isLoading: false,
    isTyping: false
  }

  ncService = ncService

  delayedCallback = _.debounce(() => {
    this.setState({ isTyping: false })
    this.search()
  }, 1000)

  search = () => {
    const { search } = this.state
    if (!search) {
      return
    }
    this.setState({ isLoading: true })
    const searchAsNumber = parseInt(search, 10)

    if (isNaN(searchAsNumber)) {
      this.ncService
        .getTransaction(search)
        .then(this.handleGetTransaction)
        .catch(this.handleFailure)
    } else {
      this.ncService
        .getBlock(searchAsNumber)
        .then(this.handleGetBlock)
        .catch(this.handleFailure)
    }
  }

  handleFailure = err => {
    console.error(err)
    this.setState({
      result: {
        block: null,
        transaction: null
      }
    })
  }

  handleGetBlock = block => {
    const height = block && block.header && block.header.height

    if (isNumber(height)) {
      this.setState({
        result: {
          block: block.header.height,
          transaction: null
        }
      })
    } else {
      this.setState({
        result: {
          block: null,
          transaction: null
        }
      })
    }
  }

  handleGetTransaction = transaction => {
    if (transaction && transaction.id && transaction.id.data) {
      this.setState({
        result: {
          block: null,
          transaction: transaction.id.data
        }
      })
    } else {
      this.setState({
        result: {
          block: null,
          transaction: null
        }
      })
    }
  }

  handleChange = e => {
    const value = e.target.value
    this.setState({ search: value })
    this.setState({ isTyping: true })
    this.delayedCallback()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.result !== this.state.result) {
      this.setState({ isLoading: false })
    }
  }

  render() {
    const {
      result: { block, transaction },
      search,
      isLoading,
      isTyping
    } = this.state

    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-start',
          flexDirection: 'column',
          alignItems: 'center',
          margin: '30px 0',
          height: 100
        }}
      >
        <MyInput
          value={this.state.search}
          onChange={this.handleChange}
          placeholder="Search block or transaction..."
        />
        {!isTyping &&
          !isLoading &&
          search &&
          !block &&
          !transaction && <NoResult>{search}</NoResult>}
        {isNumber(block) && <Redirect to={'/block/' + block} />}
        {transaction && (
          <Redirect to={'/transaction/' + encodeURI(transaction)} />
        )}
        {isLoading && <MyLoader />}
      </div>
    )
  }
}
