import { getLastBlocks, getTotalBlocks, getTotalTrans, IBlockExpRecordNew } from 'nc-utils';
import React, { Component, SFC } from 'react';
import styled from 'react-emotion';
import { timeAgo } from '../../services/time';
import { StyledLinkFancy } from '../../ui-kit/Buttons';
import { Title } from '../../ui-kit/Titles';
import { CardBorder, fadeEffect, VanillaContainer } from '../../ui-kit/Wrappers';
import { MyTd, MyTh, TableWithBorder, Total } from '../Incoming';
import Search from './Search';

const NUM_COLS = 5

const BeTh = styled(MyTh)({ width: `${100 / NUM_COLS}%` })

export interface IBlockExpRow extends IBlockExpRecordNew {
  fade?: boolean
}

const MyBexpRow: SFC<{
  record: IBlockExpRow
}> = props => {
  const { height, date, transactions, total, sender, fade } = props.record
  const MyTrl = styled('tr')(fade ? fadeEffect : {})
  return (
    <MyTrl>
      <MyTd>
        <StyledLinkFancy to={'/block/' + height}>{height}</StyledLinkFancy>
      </MyTd>

      <MyTd>{timeAgo(date)}</MyTd>
      <MyTd>{transactions}</MyTd>
      <MyTd>{total}</MyTd>
      <MyTd>{sender.slice(0, 18)}…</MyTd>
    </MyTrl>
  )
}

interface IState {
  records: IBlockExpRecordNew[]
  firstLoad: boolean
  totalTrans: number
  totalBlocks: number
}

/**
 * Sort blocks by height
 */
export function sort(data: IBlockExpRecordNew[]) {
  return data
    .sort((a, b) => {
      return a.height - b.height
    })
    .reverse()
}

export const updateBlocks = (
  prevBlocks: IBlockExpRow[],
  newBlocks: IBlockExpRow[]
) => {
  const result: IBlockExpRow[] = [...prevBlocks]

  prevBlocks = prevBlocks.map(prevB => {
    prevB.fade = false
    return prevB
  })

  /**
   * iterate through new blocks from small height to large height
   */
  sort(newBlocks as IBlockExpRow[])
    .reverse()
    .forEach((newB: IBlockExpRow) => {
      // add new block if it isn't present in previous blocks
      if (!prevBlocks.some(prevB => prevB.height === newB.height)) {
        newB.fade = true
        result.push(newB)
      }
    })

  return sort(result)
}

export default class BlockExplorer extends Component<any, IState> {
  state = {
    records: [] as IBlockExpRecordNew[],
    totalTrans: 0,
    totalBlocks: 0,
    firstLoad: false
  }

  componentDidMount() {
    this.refreshBlocks()
    this.initializeRefresh()
  }

  refreshBlocks() {
    getTotalTrans()
      .then(({ totalNbTransactions }) => {
        this.setState({ totalTrans: totalNbTransactions })
      })
      .catch(err => {
        console.error(err)
        // toastr.error('failed to load total transactions')
      })

    getTotalBlocks()
      .then(({ totalNbBlocks }) => {
        this.setState({ totalBlocks: totalNbBlocks })
      })
      .catch(err => {
        console.error(err)
        // toastr.error('failed to load total blocks')
      })

    getLastBlocks()
      .then(newRecords => {
        this.setState(currentState => ({
          records: updateBlocks(currentState.records, newRecords).slice(0, 10)
          // evenUpdate: !currentState.evenUpdate
        }))
      })
      .catch(err => {
        console.error(err)
        // toastr.error('failed to load blocks')
      })
  }

  initializeRefresh() {
    ;(this as any).refresh = setInterval(() => {
      this.refreshBlocks()
    }, 3000)
  }

  componentWillUnmount() {
    window.clearInterval((this as any).refresh)
  }

  render() {
    const { records, totalBlocks, totalTrans } = this.state

    return (
      <VanillaContainer
        style={{
          flex: 1
        }}
      >
        <Title>
          Block Explorer
          <span style={{ margin: '0 10px' }} className="ec ec-pick" />
        </Title>
        <Search />
        <Total>
          Recent activity on Neurochain:
          <br />
          Total blocks:
          {typeof totalBlocks === 'number' && totalBlocks.toString()}
          <br />
          Total transactions:
          {typeof totalTrans === 'number' && totalTrans.toString()}
        </Total>

        <CardBorder>
          <TableWithBorder>
            <tbody>
              <tr style={{ background: 'white' }}>
                <BeTh>Height</BeTh>
                <BeTh>Age</BeTh>
                <BeTh>Transactions</BeTh>
                <BeTh>Total</BeTh>
                <BeTh>Sender</BeTh>
              </tr>

              {records.map(record => {
                return <MyBexpRow key={record.height} record={record} />
              })}
            </tbody>
          </TableWithBorder>
        </CardBorder>
      </VanillaContainer>
    )
  }
}
