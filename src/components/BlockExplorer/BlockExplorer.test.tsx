import React from 'react'

import { shallow } from 'enzyme'

import BlockExplorer, {
  IBlockExpRow,
  sort,
  updateBlocks
} from './BlockExplorer'

describe('<BlockExplorer />', () => {
  it.skip('should initialize with corrent state ', () => {
    const wrapper = shallow(<BlockExplorer />)
    const instance = wrapper.instance() as BlockExplorer
    expect(instance.state).toEqual({
      firstLoad: false,
      records: []
    })
  })

  test('updateBlocks function should update blocklist correctly', () => {
    let prevBlocks: IBlockExpRow[] = []
    let newBlocks: IBlockExpRow[] = [
      { height: 2 } as IBlockExpRow,
      { height: 1 } as IBlockExpRow,
      { height: 3 } as IBlockExpRow
    ]

    let expected = [
      { height: 3, fade: true } as IBlockExpRow,
      { height: 2, fade: true } as IBlockExpRow,
      { height: 1, fade: true } as IBlockExpRow
    ]

    expect(updateBlocks(prevBlocks, newBlocks)).toEqual(expected)

    prevBlocks = [
      { height: 6, fade: true } as IBlockExpRow,
      { height: 4, fade: true } as IBlockExpRow,
      { height: 5, fade: true } as IBlockExpRow
    ]
    newBlocks = [
      { height: 7 } as IBlockExpRow,
      { height: 8 } as IBlockExpRow,
      { height: 9 } as IBlockExpRow
      // { height: 2 } as IBlockExpRow
    ]

    expected = [
      { height: 9, fade: true } as IBlockExpRow,
      { height: 8, fade: true } as IBlockExpRow,
      { height: 7, fade: true } as IBlockExpRow,
      { height: 6, fade: false } as IBlockExpRow,
      { height: 5, fade: false } as IBlockExpRow,
      { height: 4, fade: false } as IBlockExpRow
      // { height: 0 } as IBlockExpRow
    ]

    expect(updateBlocks(prevBlocks, newBlocks)).toEqual(expected)

    prevBlocks = [
      { height: 9, fade: true } as IBlockExpRow,
      { height: 8, fade: true } as IBlockExpRow,
      { height: 7, fade: true } as IBlockExpRow,
      { height: 6, fade: true } as IBlockExpRow,
      { height: 5, fade: true } as IBlockExpRow,
      { height: 4, fade: true } as IBlockExpRow
    ]
    newBlocks = []

    expected = [
      { height: 9, fade: false } as IBlockExpRow,
      { height: 8, fade: false } as IBlockExpRow,
      { height: 7, fade: false } as IBlockExpRow,
      { height: 6, fade: false } as IBlockExpRow,
      { height: 5, fade: false } as IBlockExpRow,
      { height: 4, fade: false } as IBlockExpRow
    ]

    expect(updateBlocks(prevBlocks, newBlocks)).toEqual(expected)
  })

  test('sort function should return the right value', () => {
    const input: IBlockExpRow[] = [
      { height: 2 } as IBlockExpRow,
      { height: 1 } as IBlockExpRow,
      { height: 0 } as IBlockExpRow,
      { height: 3 } as IBlockExpRow
    ]

    const expected: IBlockExpRow[] = [
      { height: 3 } as IBlockExpRow,
      { height: 2 } as IBlockExpRow,
      { height: 1 } as IBlockExpRow,
      { height: 0 } as IBlockExpRow
    ]

    expect(sort(input)).toEqual(expected)
  })
})
