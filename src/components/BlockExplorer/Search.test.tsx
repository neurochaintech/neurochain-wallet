import { shallow } from 'enzyme'
import React from 'react'
import { Redirect } from 'react-router'
import Search, { NoResult } from './Search'

describe('<Search />', () => {
  test('search should call ncService with the right arguments if search is a STRING', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search
    const mockGetBlock = jest.fn().mockReturnValue(Promise.resolve('foo'))
    const mockGetTransaction = jest.fn().mockReturnValue(Promise.resolve('foo'))

    const mockNcc = {
      getBlock: mockGetBlock,
      getTransaction: mockGetTransaction
    }

    instance.ncService = mockNcc
    instance.setState({ search: 'mySearch' })
    instance.search()

    expect(mockGetTransaction).toHaveBeenCalledWith('mySearch')
  })

  test('search should call ncService with the right arguments if search is a NUMBER', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search
    const mockGetBlock = jest.fn().mockReturnValue(Promise.resolve('foo'))
    const mockGetTransaction = jest.fn().mockReturnValue(Promise.resolve('foo'))

    const mockNcc = {
      getBlock: mockGetBlock,
      getTransaction: mockGetTransaction
    }

    instance.ncService = mockNcc
    instance.setState({ search: '3333' })
    instance.search()

    expect(mockGetBlock).toHaveBeenCalledWith(3333)
  })

  test('search should not call any service if search is blank', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search
    const mockGetBlock = jest.fn().mockReturnValue(Promise.resolve('foo'))
    const mockGetTransaction = jest.fn().mockReturnValue(Promise.resolve('foo'))

    const mockNcc = {
      getBlock: mockGetBlock,
      getTransaction: mockGetTransaction
    }

    instance.ncService = mockNcc
    instance.setState({ search: '' })
    instance.search()

    expect(mockGetBlock).not.toHaveBeenCalled()
    expect(mockGetTransaction).not.toHaveBeenCalled()
  })

  test('handleGetBlock should handle result correctly ', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search
    const mockGetBlock = jest.fn()
    const mockGetTransaction = jest.fn()

    const mockNcc = {
      getBlock: mockGetBlock,
      getTransaction: mockGetTransaction
    }

    instance.ncService = mockNcc
    instance.handleGetBlock({
      header: {
        id: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        },
        timestamp: { data: 1539641469 },
        previousBlockHash: { type: 'SHA256', data: '' },
        author: {
          type: 'ECP256K1',
          rawData:
            'O7/SXozQNkFBAgEBA0IABJSuX8hHIstQm1VxJpSsViEbelW2JuDZfoWLEjojgRPYSW1zVomTW/i6hzMnxmzob1fydaUQUaN1pdmAO4NQeRQ='
        },
        height: 7
      },
      transactions: []
    })
    // instance.search()

    expect(instance.state.result).toEqual({ block: 7, transaction: null })

    instance.ncService = mockNcc
    instance.handleGetBlock({})

    expect(instance.state.result).toEqual({ block: null, transaction: null })
  })

  test('handleGetTransaction should handle result correctly ', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search
    const mockGetBlock = jest.fn()
    const mockGetTransaction = jest.fn()

    const mockNcc = {
      getBlock: mockGetBlock,
      getTransaction: mockGetTransaction
    }

    instance.ncService = mockNcc
    instance.handleGetTransaction({
      inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
      outputs: [
        {
          address: {
            type: 'SHA256',
            data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
          },
          value: { value: '1152921504606846976' },
          data: 'dHJheCBraWxsZWQgbWU='
        }
      ],
      fees: { value: '0' },
      id: {
        type: 'SHA256',
        data: 'transId'
      },
      blockId: {
        type: 'SHA256',
        data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
      }
    })
    // instance.search()

    expect(instance.state.result).toEqual({
      block: null,
      transaction: 'transId'
    })

    instance.ncService = mockNcc
    instance.handleGetTransaction({})

    expect(instance.state.result).toEqual({ block: null, transaction: null })
  })

  it('should render correctly if there are no results', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search

    instance.setState({
      search: 'fooo',
      result: { block: null, transaction: null }
    })

    expect(wrapper.find(NoResult).exists()).toEqual(true)
  })

  it('should render correctly if nothing was typed ', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search

    instance.setState({
      search: '',
      result: { block: null, transaction: null }
    })

    expect(wrapper.find(NoResult).exists()).toEqual(false)
  })

  it('should render correctly if there is a block in result ', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search

    instance.setState({
      search: '',
      result: { block: 22, transaction: null }
    })

    expect(wrapper.find(Redirect).props().to).toEqual('/block/22')
  })

  it('should render correctly if there is a transaction in result ', () => {
    const wrapper = shallow(<Search />)
    const instance = wrapper.instance() as Search

    instance.setState({
      search: '',
      result: { block: null, transaction: 'fff' }
    })

    expect(wrapper.find(Redirect).props().to).toEqual('/transaction/fff')
  })
})
