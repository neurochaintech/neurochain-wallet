import React from 'react'
import styled from 'react-emotion'
import { HeaderBar } from './Header'

const Title = styled('h1')({
  fontFamily: "'Montserrat', sans-serif",
  margin: '0 20px'
})
const logo = require('../assets/logo-neurochain-new.png')
const NeuroTitle = () => <Title>NEUROCHAIN</Title>
const NeuroLogo = () => (
  <img height={30} src={logo} style={{ display: 'block' }} />
)

export default function HeaderMinimal() {
  return (
    <HeaderBar>
      {/*  */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%'
        }}
      >
        <NeuroLogo />
        <NeuroTitle />
      </div>
    </HeaderBar>
  )
}
