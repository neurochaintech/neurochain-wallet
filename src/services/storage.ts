const STORAGE_KEY = 'neurochain-pub-key'

export const saveKey = (key: string) => localStorage.setItem(STORAGE_KEY, key)

export const deleteKey = () => localStorage.removeItem(STORAGE_KEY)

export const getKey = () => localStorage.getItem(STORAGE_KEY)
