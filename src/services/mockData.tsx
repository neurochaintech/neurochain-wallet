const mock1 = {
  header: {
    id: {
      type: 'SHA256',
      data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
    },
    timestamp: { data: 1539641469 },
    previousBlockHash: { type: 'SHA256', data: '' },
    author: {
      type: 'ECP256K1',
      rawData:
        'MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBA0IABJSuX8hHIstQm1VxJpSsViEbelW2JuDZfoWLEjojgRPYSW1zVomTW/i6hzMnxmzob1fydaUQUaN1pdmAO4NQeRQ='
    },
    height: 0
  },
  transactions: [
    {
      inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
      outputs: [
        {
          address: {
            type: 'SHA256',
            data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
          },
          value: { value: '1152921504606846976' },
          data: 'dHJheCBraWxsZWQgbWU='
        }
      ],
      fees: { value: '0' },
      id: {
        type: 'SHA256',
        data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
      },
      blockId: {
        type: 'SHA256',
        data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
      }
    },
    {
      inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
      outputs: [
        {
          address: {
            type: 'SHA256',
            data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
          },
          value: { value: '1152921504606846976' },
          data: 'd2hhdCBvbGl2aWVyID8='
        }
      ],
      fees: { value: '0' },
      id: {
        type: 'SHA256',
        data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
      },
      blockId: {
        type: 'SHA256',
        data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
      }
    }
  ]
}

const mock2 = {
  blocks: [
    {
      header: {
        id: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        },
        timestamp: { data: 1539641469 },
        previousBlockHash: { type: 'SHA256', data: '' },
        author: {
          type: 'ECP256K1',
          rawData:
            'MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBA0IABJSuX8hHIstQm1VxJpSsViEbelW2JuDZfoWLEjojgRPYSW1zVomTW/i6hzMnxmzob1fydaUQUaN1pdmAO4NQeRQ='
        },
        height: 0
      },
      transactions: [
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
              },
              value: { value: '1152921504606846976' },
              data: 'dHJheCBraWxsZWQgbWU='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        },
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
              },
              value: { value: '1152921504606846976' },
              data: 'd2hhdCBvbGl2aWVyID8='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        }
      ]
    }
  ]
}

const mock3 = {
  blocks: [
    {
      header: {
        id: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        },
        timestamp: { data: 1539641469 },
        previousBlockHash: { type: 'SHA256', data: '' },
        author: {
          type: 'ECP256K1',
          rawData:
            'MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBA0IABJSuX8hHIstQm1VxJpSsViEbelW2JuDZfoWLEjojgRPYSW1zVomTW/i6hzMnxmzob1fydaUQUaN1pdmAO4NQeRQ='
        },
        height: 0
      },
      transactions: [
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
              },
              value: { value: '1152921504606846976' },
              data: 'dHJheCBraWxsZWQgbWU='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        },
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
              },
              value: { value: '1152921504606846976' },
              data: 'd2hhdCBvbGl2aWVyID8='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        }
      ]
    }
  ]
}
